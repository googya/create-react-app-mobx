### 构建过程
使用 create-react-app 与 mobx 并不能很好的兼容，这样会不支持 mobx 所使用的 decorator 方式， 那么解决方法有两个， 一个是不使用注解， 另一种是 使用  [custom-react-scripts](https://github.com/kitze/custom-react-scripts)， 这样就能使用 create-react-app 与 mobx 注解同时使用了



### 代码中注解识别的问题参考
[How to remove experimentalDecorators warning in VSCode](https://ihatetomatoes.net/how-to-remove-experimentaldecorators-warning-in-vscode/)

